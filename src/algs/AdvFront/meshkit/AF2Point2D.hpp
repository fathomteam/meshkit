/*
 * AF2Point2D.hpp
 *
 * An immutable point with two double coordinates.
 */

// MeshKit
#include "meshkit/Point2D.hpp"

typedef Point2D AF2Point2D;
